package main

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/exec"
	"time"

	"github.com/streadway/amqp"

	. "mq-service/lib"
)

func handleCmd(cmd *exec.Cmd) {
	out, err := cmd.CombinedOutput()
	if err != nil {
		log.Fatalf("%s", err)
	} else {
		log.Printf("succest %s", string(out))
	}
}

func handleRemoveStack(oldVersion int) {
	// 等待old client
	time.Sleep(time.Second * 15)
	cmd := exec.Command("docker", "stack", "rm", fmt.Sprintf("qsf-client-%d", oldVersion))
	handleCmd(cmd)
}

func handleUpdateReplicas(info *UpdateReplicasInfo) {
	cmd := exec.Command("docker", "service", "update", "--force", "--replicas", fmt.Sprintf("%d", info.Count), info.ServiceName)
	handleCmd(cmd)
}

func main() {
	conn, err := amqp.Dial(MQ_CONN)
	FailOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	FailOnError(err, "Failed to open a channel")
	defer ch.Close()

	err = ch.ExchangeDeclare(
		"qsf-client-message", // name
		"topic",              // type
		true,                 // durable
		false,                // auto-deleted
		false,                // internal
		false,                // no-wait
		nil,                  // arguments
	)
	FailOnError(err, "Failed to declare an exchange")

	q, err := ch.QueueDeclare(
		"",    // name
		false, // durable
		false, // delete when unused
		true,  // exclusive
		false, // no-wait
		nil,   // arguments
	)
	FailOnError(err, "Failed to declare a queue")

	if len(os.Args) < 2 {
		log.Printf("Usage: %s [binding_key]...", os.Args[0])
		os.Exit(0)
	}
	for _, s := range os.Args[1:] {
		log.Printf("Binding queue %s to exchange %s with routing key %s", q.Name, "qsf-client-message", s)
		err = ch.QueueBind(
			q.Name,               // queue name
			s,                    // routing key
			"qsf-client-message", // exchange
			false,
			nil)
		FailOnError(err, "Failed to bind a queue")
	}

	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto ack
		false,  // exclusive
		false,  // no local
		false,  // no wait
		nil,    // args
	)
	FailOnError(err, "Failed to register a consumer")

	forever := make(chan bool)

	go func() {
		for d := range msgs {
			var body Body
			if err := json.Unmarshal(d.Body, &body); err != nil {
				log.Fatalf("Body JSON unmarshaling failed: %s", err)
			}
			sDec, err := base64.StdEncoding.DecodeString(body.Message)
			if err != nil {
				log.Printf("base64 decode failure, error=[%v]\n", err)
			}
			switch body.StreamAction {
			case StreamAction.ConsulToClient:
				var myClientInfo MyClientInfo
				if err := json.Unmarshal(sDec, &myClientInfo); err != nil {
					log.Fatalf("JSON unmarshaling failed: %s", err)
				}
				log.Printf(" [x] CurrentVersion : %d", myClientInfo.CurrentVersion)
				log.Printf(" [x] OldVersion : %d", myClientInfo.OldVersion)
				handleRemoveStack(myClientInfo.OldVersion)
				forever <- false
			case StreamAction.UpdateReplicas:
				var updateReplicasInfo UpdateReplicasInfo
				if err := json.Unmarshal(sDec, &updateReplicasInfo); err != nil {
					log.Fatalf("UpdateReplicasInfo JSON unmarshaling failed: %s", err)
				}
				if err != nil {
					fmt.Println(err)
				}
				log.Printf(" [x] ServiceName : %s", updateReplicasInfo.ServiceName)
				log.Printf(" [x] Count : %d", updateReplicasInfo.Count)
				handleUpdateReplicas(&updateReplicasInfo)
				forever <- false
			}
		}
	}()

	go func() {
		time.Sleep(time.Minute * 4)
		forever <- false
	}()

	log.Printf(" [*] Waiting for logs. To exit press CTRL+C")
	<-forever
}
