module mq-service

go 1.14

require (
	github.com/fsouza/go-dockerclient v1.7.1
	github.com/go-delve/delve v1.6.0 // indirect
	github.com/magefile/mage v1.11.0 // indirect
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/mattn/go-runewidth v0.0.10 // indirect
	github.com/onsi/ginkgo v1.8.0 // indirect
	github.com/onsi/gomega v1.5.0 // indirect
	github.com/peterh/liner v1.2.1 // indirect
	github.com/pkg/profile v0.0.0-20170413231811-06b906832ed0 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	github.com/russross/blackfriday v1.6.0 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/sirupsen/logrus v1.8.0 // indirect
	github.com/spf13/cobra v1.1.3 // indirect
	github.com/streadway/amqp v1.0.0
	go.starlark.net v0.0.0-20210212215732-ebe61bd709bf // indirect
	golang.org/x/arch v0.0.0-20210127225635-455c95562d18 // indirect
	golang.org/x/sys v0.0.0-20210217105451-b926d437f341 // indirect
	gopkg.in/airbrake/gobrake.v2 v2.0.9 // indirect
	gopkg.in/gemnasium/logrus-airbrake-hook.v2 v2.1.2 // indirect
)
