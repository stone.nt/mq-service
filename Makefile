build:
	go build -o bin/emit_msg_topic ./emit_msg_topic 
	go build -o bin/receive_msg_topic ./receive_msg_topic
	go build -o bin/remove_services ./remove_services

windows:
	go build -o bin/emit_msg_topic.exe ./emit_msg_topic 
	go build -o bin/receive_msg_topic.exe ./receive_msg_topic
	go build -o bin/remove_services.exe ./remove_services	