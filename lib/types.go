package lib

// StreamAction定義
type StreamActionType struct {
	ConsulToClient      string
	RemoveQsfContainers string
	UpdateReplicas      string
}

var StreamAction = StreamActionType{
	ConsulToClient:      "CONSUL_TO_CLIENT",
	RemoveQsfContainers: "REMOVE_QSF_CONTAINERS",
	UpdateReplicas:      "UPDATE_REPLICAS",
}

// 基本body定義
type Body struct {
	StreamAction string `json:"streamAction"`
	Message      string `json:"message"`
}
