package lib

// message定義

type MyClientInfo struct {
	OldVersion     int `json:"oldVersion"`
	CurrentVersion int `json:"currentVersion"`
}

type RemoveQsfContainersInfo []string

type UpdateReplicasInfo struct {
	ServiceName string `json:"serviceName"`
	Count       int    `json:"count"`
}
