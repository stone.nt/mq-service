package main

import (
	"fmt"
	"log"
	"os"
	"strconv"

	docker "github.com/fsouza/go-dockerclient"

	"encoding/json"
	. "mq-service/lib"

	"encoding/base64"

	"github.com/streadway/amqp"
)

func main() {
	client, err := docker.NewClientFromEnv()
	FailOnError(err, "docker.NewClientFromEnv")
	if len(os.Args) < 3 {
		log.Printf("Usage: %s [binding_key]...", os.Args[0])
		os.Exit(0)
	}
	serviceName := os.Args[1]
	closeNum, err := strconv.Atoi(os.Args[2])
	FailOnError(err, "Args")
	services, err := client.ListServices(docker.ListServicesOptions{Filters: map[string][]string{"name": {serviceName}}})
	FailOnError(err, "client.ListServices")
	if len(services) == 0 {
		return
	}
	service := services[0]
	tasks, err := client.ListTasks(
		docker.ListTasksOptions{Filters: map[string][]string{
			"service":       {service.Spec.Name},
			"desired-state": {"Running"}},
		})
	FailOnError(err, "client.ListTasks")
	tasksCount := len(tasks)
	if tasksCount == 0 {
		return
	}
	containerIDs := make(RemoveQsfContainersInfo, 0, closeNum)
	for _, task := range tasks {
		if (tasksCount - task.Slot) < closeNum {
			containerIDs = append(containerIDs, task.Status.ContainerStatus.ContainerID)
		}
	}
	fmt.Println("containerIDs", containerIDs)
	sendMessage(containerIDs)
}

func sendMessage(containerIDs RemoveQsfContainersInfo) {
	conn, err := amqp.Dial(MQ_CONN)
	FailOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	FailOnError(err, "Failed to open a channel")
	defer ch.Close()

	err = ch.ExchangeDeclare(
		"qsf-client-message", // name
		"topic",              // type
		true,                 // durable
		false,                // auto-deleted
		false,                // internal
		false,                // no-wait
		nil,                  // arguments
	)
	FailOnError(err, "Failed to declare an exchange")
	messageJson, err := json.Marshal(containerIDs)
	if err != nil {
		log.Fatalf("JSON marshaling failed: %s", err)
	}
	body := Body{
		StreamAction: StreamAction.RemoveQsfContainers,
		Message:      base64.StdEncoding.EncodeToString(messageJson),
	}
	bodyJson, err := json.Marshal(body)
	FailOnError(err, "json.Marshal")
	err = ch.Publish(
		"qsf-client-message", // exchange
		"#",                  // routing key
		false,                // mandatory
		false,                // immediate
		amqp.Publishing{
			ContentType: "application/json",
			Body:        []byte(bodyJson),
		})
	FailOnError(err, "Failed to publish a message")
	log.Printf(" [x] Sent %s", bodyJson)
}
